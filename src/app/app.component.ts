import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as moment from  'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  form: FormGroup;
  isSubmit: boolean = false;

  StartDate: string;
  FinishDate: string;
  SDate: Date;
  FDate: Date

  constructor(){}

  ngOnInit() {
    this.initForm();
  }
  
  private initForm() {

    const StartDate =  new Date();
    const FinishDate = new Date();

    this.form = new FormGroup({
      StartDate: new FormControl(StartDate),
      FinishDate: new FormControl(FinishDate)
    })
  }
  
  onSubmit() {
    const { StartDate, FinishDate } = this.form.value;

    this.StartDate = moment(StartDate).format('DD.MM.YYYY HH:mm');
    this.FinishDate = moment(FinishDate).format('DD.MM.YYYY HH:mm');
    this.SDate = new Date(StartDate);
    this.FDate = new Date(FinishDate);
    this.isSubmit = true;
  }
}
