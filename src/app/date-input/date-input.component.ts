import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.css']
})
export class DateInputComponent implements OnInit   {

  timeForm: FormGroup;

  maxHours: number = 23;
  maxMinutes: number = 59;

  minValue: number = 0;
  
  @Input('form') form: FormGroup;
  @Input('controlName') controlName: string;


  constructor(
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.initTimeForm();
  }


  private initTimeForm() {
      const date: Date = this.form.get(this.controlName).value;

      const Hours = date.getHours();
      const Minutes = date.getMinutes();

      this.timeForm = this.fb.group({
        Date: [date],
        Hours: [Hours],
        Minutes: [Minutes]
      })
  }

  public setTime(val: number, timeName: string, input: HTMLInputElement) {
    const maxValue: number = this[`max${timeName}`];

    if(val < 10 && val >= this.minValue) {
      input.value = `0${val}`;
    }

    switch(true) {
      case val > maxValue: 
        this.timeForm.patchValue({ [timeName]: 0 })
        break;
      case val < this.minValue:
        this.timeForm.patchValue({ [timeName]: maxValue })
        break;
    }

    const { value } = this.timeForm.get(timeName);
    const mainFormValue: Date = this.form.get(this.controlName).value;
    
    const method: string = `set${timeName}`;

    mainFormValue[method](value);

    this.form.get(this.controlName).patchValue(mainFormValue);
    
    window.localStorage.setItem(this.controlName, `${mainFormValue}`);
  }

  public setDate(date: Date) {
    const { Hours, Minutes } = this.timeForm.value;

    date.setHours(Hours, Minutes, 0, 0);

    this.form.get(this.controlName).patchValue(date);
  }

  wheelAction() {}

}

